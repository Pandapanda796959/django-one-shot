from django.urls import path
from todos.views import todo_list_list, todo_list_detail, create_list, edit_list, todo_list_delete, create_item, edit_item


urlpatterns = [
    path("items/<int:id>/edit/", edit_item, name="todo_item_update"),
    path("items/create/", create_item, name="todo_item_create"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    path("<int:id>/edit/", edit_list, name="todo_list_update"),
    path("create/", create_list, name="create_list"),
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
]
